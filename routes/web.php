<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\RegisterController;
use App\Http\Controllers\LoginController;
use App\Http\Controllers\TransactionController;
use App\Http\Middleware\CheckAuth;

// Exclude middleware
Route::withoutMiddleware([CheckAuth::class])->group(function () {
    // default page
	Route::get('/', function () {
		return redirect('/login');
	});
	Route::get('/processTransaction', function () {
		return redirect('/login');
	});
	Route::get('/logout', function () {
		return redirect('/login');
	});
	
	// Login page
	Route::get('/login', [LoginController::class, 'showLoginForm'])->name('login');
	Route::post('/login', [LoginController::class, 'login']);

	// Register page
	Route::get('/register', [RegisterController::class, 'create'])->name('register');
	Route::post('/register', [RegisterController::class, 'store']);
});



// Middleware section -------

// default to dashboard if it comply with middleware
Route::get('/', function () {
	return redirect('/dashboard');
});
Route::get('/logout', function () {
	return redirect('/dashboard');
});

// Dashboard page
Route::get('/dashboard', [TransactionController::class, 'showDashboard'])->name('dashboard');

// Form transaction page
Route::get('/formTransaction', [TransactionController::class, 'formTransaction'])->name('formTransaction');

// Process transaction page
Route::post('/processTransaction', [TransactionController::class, 'store'])->name('processTransaction');

// Transaction history page
Route::get('/historyTransaction', [TransactionController::class, 'history'])->name('historyTransaction');

// Logout route
Route::post('/logout', [LoginController::class, 'logout'])->name('logout');
