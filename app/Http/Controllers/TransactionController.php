<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use Illuminate\View\View;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Carbon\Carbon;
use App\Lib\Helper;
use App\Models\Transaction;

class TransactionController extends Controller {

	private $table_name = 'transactions';
	private $arrayTypes = array('Topup','Transaksi');
	
	/**
        * Display dashboard.
        *
        * @return View
        */
	public function showDashboard()
	{
		$username = Auth::user()->name;
		return view('dashboard', compact('username'));
	}
	
	/**
        * Display form transaction.
        *
        * @return View
        */
	public function formTransaction()
	{
		// libs
		$Helper = new Helper();
		
		// vars
		$login_user_id = auth()->id();
		
		// Get current balance
		$current_balance = $Helper->getCurrentBalance($login_user_id);
		
		return view('formTransaction', compact('current_balance'));
	}
	
    /**
        * Store a newly created resource in storage.
        *
        * @return Response
        */
    public function store(Request $request)
	{
		// libs
		$Helper = new Helper();
		
		// vars
		$data = $request->all();
		$input_file_exists = false;
		$file_path = '';

		// validation
		$this_user_id = ( isset($data['user_id']) ? $data['user_id'] : false );
		$this_amount = ( isset($data['amount']) ? $data['amount'] : false );
		$this_description = ( isset($data['description']) ? $data['description'] : '' );
		$this_transaction_type = ( isset($data['transaction_type']) ? $data['transaction_type'] : false );
		
		if( !$this_user_id
			|| !$this_amount
			|| !$this_transaction_type ){
			return 'Failed data format.';
		}
		else{
			// check if transaction type is within enum
			if( !in_array($this_transaction_type, $this->arrayTypes) ){
				return 'Failed transaction type.';
			}
			else{
				// check balance after transaction type Transaksi
				if( $this_transaction_type == 'Transaksi' ){
					$current_balance = $Helper->getCurrentBalance($this_user_id);
					$current_balance = $current_balance-$this_amount;
					if( $current_balance<0 ){
						return redirect()->route('formTransaction');
					}
				}
				
				// check if transaction type is Topup
				if( $this_transaction_type == 'Topup' ){
					$input_file_exists = true;
				}
				
				// If file input exists, run upload
				if( $input_file_exists ){
					// Handle file upload
					$uploader = new \App\Http\Controllers\UploaderController();
					$uploadResponse = $uploader->upload($request);

					if( $uploadResponse->getData()->success ){
						$file_path = $uploadResponse->getData()->success;
					}
					else{
						return $uploadResponse->getData()->error;
					}
				}
				
				// generate a transaction code
				$this_transaction_code = $Helper->generateTransactionCode();
				
				// DB register
				$db_data = array(
					'user_id' => $this_user_id,
					'amount' => $this_amount,
					'description' => $this_description,
					'transaction_type' => $this_transaction_type,
					'file_path' => $file_path,
					'transaction_code' => $this_transaction_code,
					'created_at' => Carbon::now(),
					'updated_at' => Carbon::now(),
				);

				$id = DB::table($this->table_name)->insertGetId($db_data);

				// return checks
				if( $id !== false ){
					// update user current balance
					$calc_result = $Helper->calcCurrentBalance($this_user_id);
					if( $calc_result ){
						// success result
						return redirect()->route('historyTransaction');
					}
					else{
						return 'Failed updating balance.';
					}
				}
				else{
					return 'Failed DB register.';
				}
			}
		}
	}
	
	/**
        * Display a listing of the resource.
        *
        * @return View
        */
	public function history(Request $request)
    {
		// libs
		$Helper = new Helper();
		
		// vars
		$login_user_id = auth()->id();
		$username = Auth::user()->name;
		
        // Get the search term
        $search = $request->input('search');

        // Query the transactions table
        $query = Transaction::query();

        // If a search term is provided, filter the query
		$query->where('user_id', auth()->id())
			->where(function ($query) use ($search) {
			  if( $search ){
				  $query->where('description', 'like', '%' . $search . '%')
						->orWhere('transaction_code', 'like', '%' . $search . '%');
			  }
		});
		
		// Filter by transaction type
		if( $transactionType = $request->input('transaction_type') ){
			$query->where('transaction_type', $transactionType);
		}
		
        // Paginate the results with sorting by created_at in descending order
		$transactions = $query->orderBy('created_at', 'desc')->paginate(5);
		
		// Get current balance
		$current_balance = $Helper->getCurrentBalance($login_user_id);
		
        // Pass the paginated results to the view
        return view('historyTransaction', [
			'transactions' => $transactions,
			'current_balance' => $current_balance,
			'username' => $username,
		]);
    }
}