<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class RegisterController extends Controller
{
    public function create()
    {
        return view('registration');
    }

    public function store(Request $request)
    {
		$request->validate([
			'name' => 'required|string|max:100',
			'email' => [
				'required',
				'string',
				'email',
				'max:255',
				'unique:users',
				function ($attribute, $value, $fail) {
					$atPosition = strpos($value, '@');
					$dotPosition = strrpos($value, '.'); // Get the position of the last dot

					if( $atPosition !== false && $dotPosition !== false ) {
						$afterDot = substr($value, $dotPosition + 1); // Get the string after the last dot
						
						if( empty($afterDot) ) { // Check if there's a string after the dot
							return $fail('The '.$attribute.' must have a string after the dot.');
						}
					}
					else{
						return $fail('The '.$attribute.' must have a dot after the @ symbol.');
					}
				},
			],
			'password' => [
				'required',
				'string',
				'min:8', // Minimum 8 characters
				'confirmed',
				'regex:/[A-Z]/', // At least one uppercase letter
				'regex:/[0-9]/', // At least one number
				'regex:/[@$!%*#?&]/', // At least one special character
			],
		]);

        User::create([
            'name' => $request->name,
            'email' => $request->email,
            'password' => Hash::make($request->password),
        ]);

        return redirect()->route('login');
    }
}
