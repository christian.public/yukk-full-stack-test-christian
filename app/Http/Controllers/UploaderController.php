<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Carbon\Carbon;

class UploaderController extends Controller
{
	private $storage_path = 'public/files';
	private $laravel_local_path = 'storage/app';
	
    public function upload(Request $request)
    {
        if($request->hasFile('file')) {
            // Get filename with the extension
            $filenameWithExt = $request->file('file')->getClientOriginalName();

            // Get just filename
            $filename = pathinfo($filenameWithExt, PATHINFO_FILENAME);

            // Get just extension
            $extension = $request->file('file')->getClientOriginalExtension();

            // Filename to store
            $fileNameToStore = $filename.'_'.time().'.'.$extension;
			
			// Separate folders monthly
			$this_storage_path = $this->storage_path.'/'.Carbon::now()->format('Ym');
			
            // Upload the image
            $path = $request->file('file')->storeAs($this_storage_path, $fileNameToStore);
			
			// recover laravel local path
			$local_path = $this->laravel_local_path.'/'.$path;

            return response()->json(['success' => $local_path]);
        } else {
            return response()->json(['error' => 'No file selected for uploading.']);
        }
    }
}
