<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class CheckAuth
{
    public function handle($request, Closure $next)
    {
        if (!Auth::check()) {
            // If the user is not authenticated, redirect to the login page
            return redirect('login');
        }

        // If the user is authenticated, proceed with the request
        return $next($request);
    }
}
