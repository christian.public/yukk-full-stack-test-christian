<?php

namespace App\Lib;

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;
use App\Models\Transaction;
use App\Models\Users;

class Helper {
	
	/**
        * Generate a 12-digit transaction code.
        *
        * @return String
        */
	public function generateTransactionCode(){
		$date = date('Ymd');
		$randomString = Str::random(4);
		
		do {
			$randomString = Str::random(4);
			$newCode = $date . $randomString;
		} while (Transaction::where('transaction_code', $newCode)->exists());

		return $newCode;
	}
	
	/**
        * Get current user balance from table Users.
        *
        * @return Double
        */
	public function getCurrentBalance($user_id){
		$balance = DB::table('Users')
			->where('id', $user_id)
			->value('current_balance');

		return $balance;
	}
	
	/**
        * Calculate user balance based on transactions in transactions table.
        *
        * @return Condition
        */
	public function calcCurrentBalance($user_id){
		$credit = DB::table('transactions')
			->where('transaction_type', 'Topup')
			->where('user_id', $user_id)
			->sum('amount');
		$debit = DB::table('transactions')
			->where('transaction_type', 'Transaksi')
			->where('user_id', $user_id)
			->sum('amount');

		$balance = ( $credit-$debit<0 ? 0 : $credit-$debit );

		$affected = DB::table('Users')
			->where('id', $user_id)
			->update(['current_balance' => $balance]);

		if( $affected>0 ){
			return true;
		}
		else{
			return false;
		} 
	}

}