<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('transactions', function (Blueprint $table) {
            $table->increments('id');
			$table->foreignId('user_id');
			$table->double('amount',15,2)->unsigned();
			$table->text('description');
			$table->enum('transaction_type', ['Topup','Transaksi']);
			$table->string('transaction_code',12)->unique();
			$table->text('file_path');
			$table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('transactions');
    }
};
