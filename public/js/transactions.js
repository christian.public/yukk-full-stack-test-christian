window.onload = function() {
	// show or remove file upload based on chosen transaction type
	document.getElementById('topup').addEventListener('change', function() {
        if (this.checked) {
            document.getElementById('fileUpload').style.setProperty('display', 'flex', 'important');
        }
    });
    document.getElementById('transaksi').addEventListener('change', function() {
        if (this.checked) {
            document.getElementById('fileUpload').style.display = 'none';
        }
    });
	
	// make it mandatory file upload if transaction is a Topup
    var topupRadio = document.getElementById('topup');
    var fileInput = document.getElementById('file');

    topupRadio.addEventListener('change', function() {
        if (this.checked) {
            document.getElementById('fileUpload').style.setProperty('display', 'flex', 'important');
            fileInput.required = true; // Make file input mandatory
        }
    });

    var transaksiRadio = document.getElementById('transaksi');
    transaksiRadio.addEventListener('change', function() {
        if (this.checked) {
            document.getElementById('fileUpload').style.display = 'none';
            fileInput.required = false; // Make file input not mandatory
        }
    });
};
