<!DOCTYPE html>
<html>
<head>
    <title>Login Page</title>
    <link rel="stylesheet" href="{{ asset('css/style.css') }}">
</head>
<body>
    <div class="container">
        <h2>Welcome to YUKK</h2>

        @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif

        <form method="POST" action="{{ route('login') }}" class="form-group">
            @csrf

            <div class="form-group flex-group">
                <label class="flex-label" for="email">Email:</label>
                <input class="flex-input" type="email" name="email" id="email" required>
            </div>
			
			<div class="form-group flex-group">
                <label class="flex-label" for="password">Password:</label>
                <input class="flex-input" type="password" name="password" id="password" required>
            </div>

            <div class="form-group">
                <button class="btn btn-primary" type="submit">Login</button>
            </div>
        </form>
		
		<div class="center-text">
			<p>or</p>
		</div>
		
        <div class="form-group">
            <button class="btn btn-primary" onclick="window.location.href='{{ route('register') }}'">Register</button>
        </div>
    </div>
</body>
</html>
