<!DOCTYPE html>
<html>
<head>
    <title>Transaction History Page</title>
    <link rel="stylesheet" href="{{ asset('css/style.css') }}">
</head>
<body>
    <div class="container limited-size">
		<div class="header">
			<h2>{{ $username }} Transaction History</h2>
			<div class="button-group">
				<form method="GET" action="{{ route('dashboard') }}" class="encase-btn-secondary">
					@csrf
					<button class="btn btn-secondary" type="submit">Back</button>
				</form>
				<form method="GET" action="{{ route('formTransaction') }}" class="encase-btn-primary">
					@csrf
					<button class="btn btn-primary" type="submit">Make a Transaction</button>
				</form>
			</div>
		</div>
		
		 <!-- Current Balance -->
		<p>Current Balance: {{ number_format($current_balance, 0, ',', '.') }}</p>
		
        <!-- Search form -->
        <form method="GET" action="{{ route('historyTransaction') }}">
            <input class="input-search" type="text" name="search" placeholder="Search transactions" value="{{ request('search') }}">
            <button class="btn btn-tertiary" type="submit">Search</button>
        </form>
		
        <!-- Transaction list -->
        @if($transactions->count())
            <table>
                <thead>
                    <tr>
						<th>Transaction Code</th>
                        <th>Amount</th>
                        <th>Description</th>
                        <th>
							<form method="GET" action="{{ route('historyTransaction') }}">
								<select name="transaction_type" onchange="this.form.submit()">
									<option value="">All Transaction Types</option>
									<option value="Topup" {{ request('transaction_type') == 'Topup' ? 'selected' : '' }}>Topup</option>
									<option value="Transaksi" {{ request('transaction_type') == 'Transaksi' ? 'selected' : '' }}>Transaksi</option>
								</select>
							</form>
						</th>
                        <th>Created At</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($transactions as $transaction)
                        <tr>
                            <td>{{ $transaction->transaction_code }}</td>
							<td class="right-align-column">{{ number_format($transaction->amount, 0, ',', '.') }}</td>
                            <td>{{ $transaction->description }}</td>
                            <td>{{ $transaction->transaction_type }}</td>
                            <td>{{ $transaction->created_at }}</td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
			
            <!-- Pagination links -->
            {{ $transactions->links() }}
        @else
            <p>No transactions found.</p>
        @endif
    </div>
</body>
</html>
