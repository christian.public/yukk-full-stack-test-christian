<!DOCTYPE html>
<html>
<head>
    <title>Dashboard Page</title>
    <link rel="stylesheet" href="{{ asset('css/style.css') }}">
</head>
<body>
    <div class="container">
        <div class="header">
			<h2>Welcome, {{ $username }}</h2>
			<form method="POST" action="{{ route('logout') }}" class="logout-form">
				@csrf
				<button class="btn btn-secondary" type="submit">Logout</button>
			</form>
		</div>

        <!-- Menu -->
        <ul>
            <li><a href="{{ route('formTransaction') }}">Transaction</a></li>
            <li><a href="{{ route('historyTransaction') }}">History</a></li>
        </ul>
		
    </div>
</body>
</html>
