<!DOCTYPE html>
<html>
<head>
    <title>Registration Page</title>
    <link rel="stylesheet" href="{{ asset('css/style.css') }}">
</head>
<body>
	@extends('layouts.app')

	@section('content')
	<div class="container">
		<div class="header">
			<h2>Become a YUKK member</h2>
			<form method="GET" action="{{ route('login') }}" class="encase-btn-secondary">
				@csrf
				<button class="btn btn-secondary" type="submit">Back</button>
			</form>
        </div>
		
		<div class="row justify-content-center">
			<div class="col-md-8">
				<div class="card">
					<!-- div class="card-header">{{ __('Register') }}</div -->

					<div class="card-body">
						<form method="POST" action="{{ route('register') }}" class="form-group">
							@csrf

							<div class="form-group flex-group">
								<label for="name" class="flex-label col-md-4 col-form-label text-md-right">{{ __('Name') }}</label>
								<div class="col-md-6">
									<input id="name" type="text" class="flex-input vform-control @error('name') is-invalid @enderror" name="name" value="{{ old('name') }}" required autocomplete="name" autofocus>

									@error('name')
										<span class="invalid-feedback alert" role="alert">
											<strong>{{ $message }}</strong>
										</span>
									@enderror
								</div>
							</div>

							<div class="form-group flex-group">
								<label for="email" class="flex-label col-md-4 col-form-label text-md-right">{{ __('E-Mail') }}</label>
								<div class="col-md-6">
									<input id="email" type="email" class="flex-input form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email">

									@error('email')
										<span class="invalid-feedback alert" role="alert">
											<strong>{{ $message }}</strong>
										</span>
									@enderror
								</div>
							</div>

							<div class="form-group flex-group">
								<label for="password" class="flex-label col-md-4 col-form-label text-md-right">{{ __('Password') }}</label>
								<div class="col-md-6">
									<input id="password" type="password" class="flex-input form-control @error('password') is-invalid @enderror" name="password" required autocomplete="new-password">

									@error('password')
										<span class="invalid-feedback alert" role="alert">
											<strong>{{ $message }}</strong>
										</span>
									@enderror
								</div>
							</div>

							<div class="form-group flex-group">
								<label for="password-confirm" class="flex-label col-md-4 col-form-label text-md-right">{{ __('Confirm Password') }}</label>
								<div class="col-md-6">
									<input id="password-confirm" type="password" class="flex-input form-control" name="password_confirmation" required autocomplete="new-password">
								</div>
							</div>

							<div class="form-group mb-0">
								<div class="col-md-6 offset-md-4">
									<button type="submit" class="btn btn-primary">
										{{ __('Register') }}
									</button>
								</div>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>
	@endsection
</body>
</html>
