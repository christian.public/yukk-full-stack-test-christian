<!DOCTYPE html>
<html>
<head>
    <title>Form Transaction</title>
    <link rel="stylesheet" href="{{ asset('css/style.css') }}">
    <script src="{{ asset('js/transactions.js') }}"></script>
</head>
<body>
    <div class="container reduced-size">
        <div class="header">
			<h2>Form Transaction</h2>
			<form method="GET" action="{{ route('dashboard') }}" class="encase-btn-secondary">
				@csrf
				<button class="btn btn-secondary" type="submit">Back</button>
			</form>
        </div>
		
		<!-- Current Balance -->
        <p>Current Balance: {{ number_format($current_balance, 0, ',', '.') }}</p>
		
        <!-- Input fields for Amount and Description -->
        <form action="{{ route('processTransaction') }}" method="POST" enctype="multipart/form-data" class="form-group-form-transaction">
            @csrf

            <!-- Hidden field for the user ID -->
            <input type="hidden" name="user_id" value="{{ auth()->id() }}">

            <!-- Radio buttons for Topup and Transaksi -->
			<div class="radio-group">
				<label>
					<input type="radio" id="topup" name="transaction_type" value="Topup">
					Topup
				</label>
				<label>
					<input type="radio" id="transaksi" name="transaction_type" value="Transaksi">
					Transaksi
				</label>
			</div>
			
			<!-- Amount label and input side by side -->
			<div class="form-group-form-transaction flex-group-form-transaction">
				<label for="amount" class="flex-label-form-transaction">Amount:</label>
				<input type="number" id="amount" name="amount" min="0" step="0.01" required class="flex-input-form-transaction">
			</div>
			
			<!-- Description label and input side by side -->
			<div class="form-group-form-transaction flex-group-form-transaction">
				<label for="description" class="flex-label-form-transaction">Description:</label>
				<input type="text" id="description" name="description" required class="flex-input-form-transaction">
			</div>
			
            <!-- File uploader -->
            <div id="fileUpload" style="display: none;" class="form-group-form-transaction flex-group-form-transaction">
                <label class="flex-label-form-transaction" for="file">Upload file:</label>
                <input class="flex-input-file-upload" type="file" id="file" name="file">
            </div>

            <input type="submit" value="Submit" class="btn-primary">
        </form>
		
    </div>
</body>
</html>
