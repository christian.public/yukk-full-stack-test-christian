<?php

use Illuminate\Support\Facades\Route;
use Illuminate\Foundation\Application;
use Illuminate\Foundation\Configuration\Exceptions;
use Illuminate\Foundation\Configuration\Middleware;
use App\Http\Middleware\CheckAuth;

return Application::configure(basePath: dirname(__DIR__))
	->withRouting(
		commands: __DIR__.'/../routes/console.php',
		using: function () {
			Route::middleware('web')
				->namespace('App\Http\Controllers')
				->group(base_path('routes/web.php'));
		},
		health: '/up',
	)
    ->withMiddleware(function (Middleware $middleware) {
		$middleware->web(append: [
			CheckAuth::class,
		]);
    })
    ->withExceptions(function (Exceptions $exceptions) {
        //
    })->create();
